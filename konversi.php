<!DOCTYPE html>
<html>
<head>
    <title>Konversi Nilai</title>
</head>
<body>
    <h1>Konversi Nilai</h1>
    <form method="post" action="">
        Masukkan Nilai Angka: <input type="number" name="nilai" required>
        <input type="submit" name="submit" value="Konversi">
    </form>

    <?php
    if (isset($_POST['submit'])) {
        $nilai = $_POST['nilai'];
        
        if ($nilai >= 86 && $nilai <= 100) {
            $huruf = "A";
            $ipk = 4.0;
            $keterangan = "LULUS";
        } elseif ($nilai >= 81 && $nilai <= 85) {
            $huruf = "A-";
            $ipk = 3.7;
            $keterangan = "LULUS";
        } elseif ($nilai >= 76 && $nilai <= 80) {
            $huruf = "B+";
            $$ipk = 3.3;
            $keterangan = "LULUS";
        } elseif ($nilai >= 71 && $nilai <= 75) {
            $huruf = "B";
            $ipk = 3.0;
            $keterangan = "LULUS";
        } elseif ($nilai >= 66 && $nilai <= 70) {
            $huruf = "B-";
            $ipk = 2.7;
            $keterangan = "LULUS";
        } elseif ($nilai >= 61 && $nilai <= 65) {
            $huruf = "C+";
            $ipk = 2.3;
            $keterangan = "LULUS";
        } elseif ($nilai >= 56 && $nilai <= 60) {
            $huruf = "C";
            $ipk = 2.0;
            $keterangan = "LULUS";
        } elseif ($nilai >= 51 && $nilai <= 55) {
            $huruf = "C-";
            $ipk = 1.7;
            $keterangan = "TIDAK LULUS";
        } elseif ($nilai >= 46 && $nilai <= 50) {
            $huruf = "D";
            $ipk = 1.0;
            $keterangan = "TIDAK LULUS";
        } else {
            $huruf = "E";
            $ipk = 0.0;
            $keterangan = "TIDAK LULUS";
        }
    
        echo "Nilai Anda $nilai <br> Huruf : $huruf  <br> ipk : $ipk  <br> Keterangan : $keterangan";
    }
    
    ?>

    <h2>Konversi Segitiga</h2>
    <form method="post" action="">
        Masukkan Tinggi Segitiga: <input type="number" name="tinggi" required>
        <input type="submit" name="Submit" value="Konversi">
    </form>
    <?php
    if (isset($_POST['Submit'])) {
        $tinggi = $_POST['tinggi'];
        echo "<pre>";
        for ($baris = 1; $baris <= $tinggi; $baris++) {
            for ($i = 1; $i <= $tinggi - $baris; $i++) {
                echo " ";
            }
            for ($j = 1; $j < 2 * $baris; $j++) {
                echo "*";
            }
            echo "\n";
        }
        
    }
    ?>
</body>
</html>
